﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour
{
    public GameObject[] objects = new GameObject[5];
    public Vector3 spawnPoint = new Vector3(-34f,0.5f,-2.49f);
    public bool is_enable = true;
    public Canvas p_menu;
  //  public Vector3 pullPoint = new Vector3(-34f,0.5f,-2.49f);

    GameObject element;
    // Start is called before the first frame update
    void Start()
    {
        p_menu.gameObject.SetActive(false);
        StartCoroutine(Test());

    }

    // Update is called once per frame
    void Repeat() //кнопка обратится сюда
    {
        StartCoroutine(Test());
    }
    //void StartSpawn()
    //{
    //    foreach (GameObject el1 in objects)
    //    {
    //        el1.GetComponent<rockmuvement>().move = true;
    //    }
    //    Repeat();
    //}

    IEnumerator Test()
    {


        if (is_enable)
        {
            yield return new WaitForSeconds(1f);
            while (true)
            {
                element = objects[Random.Range(0, objects.Length)];
                if (element.GetComponent<rockmuvement>().move == false) break;
            }

            element.transform.position = spawnPoint;
            element.GetComponent<rockmuvement>().move = true;
            Repeat();
        }
    }

    private void Update()
    {
        foreach (GameObject el in objects)
        {
            if (el.GetComponent<rockmuvement>().is_collide)
            {
                foreach (GameObject el1 in objects)
                {
                    el1.GetComponent<rockmuvement>().move = false;
                    p_menu.gameObject.SetActive(true);

                }
                is_enable = false;
                break;
            }

        }

    }
}
