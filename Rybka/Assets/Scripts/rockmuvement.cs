﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rockmuvement : MonoBehaviour
{
    public bool move = false;
    public bool is_collide = false; //определяет конец игры 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (move)  transform.position = new Vector3(transform.position.x + 0.2f, transform.position.y, transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pull"))
        {
            move = false;
        }
        if (other.CompareTag("Player"))
        {
            is_collide = true;
        }
    }

}
