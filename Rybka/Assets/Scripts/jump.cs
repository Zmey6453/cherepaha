﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jump : MonoBehaviour
{
    public bool is_ground = true;     //переменная которая хранит в себе значение, "на земле ли игрок"
    Rigidbody player;         //так как мы часть обращаемся к физике, то не лишним будет закэшировать этот компонент
    public float force;     //сила с которой будет прыгать персонаж

    void Start()
    {
        player = GetComponent<Rigidbody>(); //при старке сцены, получаем компонент и сохраняем в переменную
    }

    void OnTriggerEnter(Collider col)
    {               //если в тригере что то есть и у обьекта тег "ground"
        if (col.tag == "ground")
        {
            is_ground = true;      //то включаем переменную "на земле"
            player.isKinematic = true;
            player.useGravity = false;
            
        }
  
    }
    void OnTriggerExit(Collider col)
    {              //если из триггера что то вышло и у обьекта тег "ground"
        if (col.tag == "ground")
        {
            is_ground = false;     //то вЫключаем переменную "на земле"
            player.isKinematic = false;
            player.useGravity = true;

        }

    }


    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) & is_ground)
        {
            Debug.Log("spf");         
                is_ground = false;     //то вЫключаем переменную "на земле"
                player.isKinematic = false;
                player.useGravity = true;
               
                player.AddForce(Vector3.up * force, ForceMode.Impulse);
            
        }


        //if (Input.GetKeyDown(KeyCode.Space) && is_ground);
        //{                //если нажата кнопка "пробел" и игрок на земле
        //    player.AddForce(Vector3.up , ForceMode.Impulse);
        //}   //то придаем ему силу вверх импульсным пинком
    }
}